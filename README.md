# Cartalk Puzzlers

This repository contains solutions to the puzzlers on cartalk.com.

## Purpose

In the book "Think Python 2nd edition", there's a reference to some puzzlers on cartalk.com.  I have made this repo for fun basically, but also because I'm a Python teacher.  I want to solve the puzzlers using Python and do this in the most performant way possible.  And share my insights with students.  Or even be corrected if somebody has a better solution.

## Puzzlers coded so far

### 2021

#### November

- Number Sequence Encore

#### October

- A Young Mathematician

#### July

- What Do These Eight Words Have in Common?

#### March

- The Farmer's Stone
