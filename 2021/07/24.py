###
#
# What Do These Eight Words Have in Common?
#
# https://www.cartalk.com/radio/puzzler/what-do-these-eight-words-have-common-0
#
# Yves Vindevogel
# 2022-01-28
#

import timeit


def voodoo_word(w):
    return w == w[0] + w[:0:-1]


if __name__ == "__main__":
    start_ts = timeit.default_timer()

    for word in ["voodoo", "banana", "dresser", "grammar", "potato", "revive", "uneven"]:
        assert voodoo_word(word)

    assert not voodoo_word("yves")

    with open("../../words.txt", "r") as input_file:
        words = input_file.readlines()

    for word in words:
        if voodoo_word(word):
            print(word)

    end_ts = timeit.default_timer()

    print(end_ts - start_ts)
