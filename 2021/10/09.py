###
#
# The Young Mathematician
#
# https://www.cartalk.com/radio/puzzler/young-mathematician
#
# Yves Vindevogel
# 2022-01-28
#

###
#
# The idea behind the code, based on the logic by Carl Friedrich Gauss
#
# To calculate the number of elements between a start value and end value, the formula is:
# number_of_elements = end_value - start_value + 1
#
# e.g.  1 -> 10  => 10 - 1 + 1 = 10
#       2 -> 10  => 10 - 2 + 1 = 9
#       0 -> 10  => 10 - 0 + 1 = 11
#      -1 -> 10  => 10 -(-1) + 1 = 12
#
#
# If you have to add the number 1 to 10, you can make pairs.
# 1 + 10 = 11, 2 + 9 = 11, 3 + 8 = 11, 4 + 7 = 11, 5 + 6 = 11
#
# The value of the pair is the start value plus the end value
#
# pair_value = start_value + end_value
#
# The number of pairs you can make when there's an even number of elements is the number of elements divided by 2
# As the number of elements is already determined above, it becomes:
#
# pair_count = (end_value - start_value + 1) / 2
#
# As you have an even number of elements, one must be odd and one must be even.  The sum is odd, but you add 1,
# assuring you always have an even number, which you can divide by 2 without fraction
#
# Hence, for an even number of elements, the result of the running sum is:
#
# result = pair_value * pair_count
# =>
# result = (start_value + end_value) * (end_value - start-value + 1) / 2
#
# However, what happens if we have an odd number of elements ?
# You can make pairs, but the middle element will find no pair.
#
# e.g.  1 -> 5 => 1 + 5, 2 + 4, 3 left behind
#
# The value of the pairs is the same calculation as for an even amount of numbers
#
# pair_value = start_value + end_value
#
# To calculate the number of pairs, you can use this formula
#
# pair_count = (end_value - start_value) / 2
#
# e.g.  1 -> 5 => (5 - 1) / 2 = 2
#       1 -> 7 => (7 - 1) / 2 = 3
#       2 -> 8 => (8 - 2) / 2 = 3
#       0 -> 2 => (2 - 0) / 2 = 1
#      -1 -> 1 => (1 - (-1)) / 2 = 1
#
# As we have an odd number of elements, both start and end value will either be odd or even,
# making the division without fraction
#
# To calculate the middle number, which is not paired, the formula is:
#
# middle_value = (start_value + end_value) / 2
#
# e.g.  1 -> 5 => (1 + 5) / 2 = 3
#       1 -> 7 => (1 + 7) / 2 = 4
#       2 -> 8 => (2 + 8) / 2 = 5
#       0 -> 2 => (0 + 2) / 2 = 1
#      -1 -> 1 => (-1 + 1) / 2 = 0
#
# So, for an odd number of elements, the running sum is the number of pairs times the value of the pair,
# plus the middle number
#
# result = (pair_value * pair_count) + middle_value
# =>
# result = ((start_value + end_value) * ((end_value - start_value) / 2)) + ((start_value + end_value) / 2)
#
# Now, let's substitute some things to make this formula simpler
#
# A = start_value + end_value = end_value + start_value
# B = end_value - start_value
#
# result = A * (B / 2) + (A / 2)
# =>
# result = (B / 2) * A + (1 / 2) * A
# =>
# result = ((B / 2) + (1 / 2)) * A
# =>
# result = ((B + 1) / 2) * A
# =>
# result = A * (B + 1) / 2
# =>
# result = (start_value + end_value) * (end_value - start_value + 1) / 2
#
# For even we had:
#
# result = (start_value + end_value) * (end_value - start-value + 1) / 2
#
# Indeed, exactly the same !
#
# So, no need to make a difference between odd and even amount of numbers
#

def running_sum(start, end):
    assert start < end, "Give the numbers lowest first"

    return (start + end) * (end - start + 1) / 2


if __name__ == "__main__":
    assert running_sum(1, 100) == 5050  # Carl Friedrich Gauss
    assert running_sum(1, 6) == 21  # eyes on a dice

    # even amount of numbers
    assert running_sum(10, 11) == 21
    assert running_sum(10, 13) == 46

    # odd amount of numbers
    assert running_sum(1, 3) == 6
    assert running_sum(2, 4) == 9
    assert running_sum(1, 5) == 15

    # zero and negative numbers
    assert running_sum(0, 2) == 3
    assert running_sum(-1, 1) == 0
    assert running_sum(-3, -1) == -6

