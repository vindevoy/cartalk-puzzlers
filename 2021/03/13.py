###
#
# The Farmer's Stone
#
# https://www.cartalk.com/radio/puzzler/farmers-stone-0
#
# Yves Vindevogel
# 2022-01-28
#
#
# The logic behind the code is as follows
#
# On the left side, you will take 1, 2, 3 or 4 stones (0 is not possible)
# On the right side, as balance, you will take either 0 stones, 1 if you took between 1 and 3 stones on the left,
# 2 if you took between 1 and 2 stones on the left or 3 if you took 1 on the left
#
# The above logic is easy to loop and is done in count_left and count_right
#
# For the number of stones on the left, you search all combinations through the itertools
# Corresponding, you see what stones are possible on the right side (you can only use a stone once) and you make
# the combinations there
#
# Then you calculate the weight of left and right.  If right is higher, you skip it.  You will find the inverse
# combination also when you change left and right side.
#
# For deleting the stones in the balanced stones (see function), we need to calculate the index of
# the element to remove.  It's a list, which works on indexes only.  1 = 3 ^ 0, 3 = 3 ^ 1, 9 = 3 ^ 2, 27 = 3 ^3
# That nicely corresponds to our indexes and can be found by using the log() function with base 3.
#
# Finally, there's a little check on duplicates, which will tell you there aren't any
# And you will see all values are covered
#

from itertools import combinations
import math
import timeit


def balance_stones(left_stones):
    right_stones = stones[:]

    for stone in reversed(left_stones):
        del right_stones[int(math.log(stone, 3))]

    return right_stones


if __name__ == "__main__":
    start_ts = timeit.default_timer()

    stones = [1, 3, 9, 27]
    how_tos = {}

    for count_left in range(1, 5):
        for count_right in range(0, 5 - count_left):
            # print("PHASE: left: {0} - right: {1}".format(count_left, count_right))

            left_combinations = combinations(stones, count_left)

            for left in left_combinations:
                # print("Left stones: {0}".format(left))

                right_combinations = combinations(balance_stones(left), count_right)

                for right in right_combinations:
                    # print("Right stones: {0}".format(right))

                    weight_left = sum(left)
                    weight_right = sum(right)
                    balanced_weight = weight_left - weight_right

                    if balanced_weight > 0:
                        if how_tos.get(balanced_weight) is not None:
                            print("Duplicate for {0}".format(balanced_weight))

                        how_tos[balanced_weight] = {'left': list(left), 'right': list(right)}
                        # tuple from combinations to list

    for how_to in sorted(how_tos):
        left = how_tos[how_to]['left']
        right = how_tos[how_to]['right']

        print("{0}".format(how_to), end="kg => left: ")

        while len(left) > 0:
            lft = left.pop(0)

            print(lft, end=" kg ")

            if len(left) > 0:
                print("+ ", end="")

        if len(right) > 0:
            print("- right: ", end="")

        while len(right) > 0:
            rgt = right.pop(0)

            print(rgt, end=" kg ")

            if len(right) > 0:
                print("+ ", end="")

        print("")

    assert len(how_tos) == 40

    end_ts = timeit.default_timer()

    print("")
    print(end_ts - start_ts)
