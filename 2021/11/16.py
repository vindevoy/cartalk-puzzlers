###
#
# Number Sequence Encore
#
# https://www.cartalk.com/radio/puzzler/number-sequence-encore
#
# Yves Vindevogel
# 2022-01-27
#

import timeit


def next_sequence(sq_number: str) -> str:
    assert sq_number.isdigit(), "You must provide an integer"

    lst = list(sq_number)

    next_number = ""
    char_found = lst.pop(0)
    num_found = 1

    while len(lst) > 0:
        new_found = lst.pop(0)

        if new_found == char_found:
            num_found += 1
        else:
            next_number += str(num_found) + char_found

            char_found = new_found
            num_found = 1

    next_number += str(num_found) + char_found

    return next_number


if __name__ == "__main__":
    start_ts = timeit.default_timer()

    # We are doing string manipulations on it, not math
    number = "1"
    print(number)

    for _ in range(10):
        number = next_sequence(number)
        print(number)

    end_ts = timeit.default_timer()

    print(end_ts - start_ts)
